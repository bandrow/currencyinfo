package ru.lid.pet.bank.db.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Table("bank.currency_info")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CurrencyInfoEntity implements Persistable<Long> {
    @Id
    private Long id;
    private String country;
    private String currency;
    private String code;

    @Transient
    private boolean newCurrency;

    @Override
    public boolean isNew() {
        return newCurrency;
    }
}
