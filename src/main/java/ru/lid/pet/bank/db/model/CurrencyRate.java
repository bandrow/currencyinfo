package ru.lid.pet.bank.db.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDate;

@Table("bank.currency_rate")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CurrencyRate implements Persistable<Long> {
    @Id
    private Long id;

    private LocalDate date;
    private Integer amount;
    private String code;

    private Double rate;
    @Column("currency_info_id")
    private Long currencyInfoId;

    @Transient
    private Boolean currencyRateNew;

    @Override
    public boolean isNew() {
        return currencyRateNew;
    }
}
