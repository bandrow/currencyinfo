package ru.lid.pet.bank.db.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ru.lid.pet.bank.db.model.CurrencyRate;

import java.time.LocalDate;

@Repository
public interface CurrencyRateRepository extends ReactiveCrudRepository<CurrencyRate, Long> {
    Mono<CurrencyRate> findCurrencyRateByDateAndCodeAndAmount(LocalDate date, String code, Integer amount);
}
