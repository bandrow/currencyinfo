package ru.lid.pet.bank.db.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ru.lid.pet.bank.db.model.CurrencyInfoEntity;

@Repository
public interface CurrencyInfoRepository extends ReactiveCrudRepository<CurrencyInfoEntity, Long> {
    Mono<CurrencyInfoEntity> findCurrencyInfoEntityByCode(String code);
}
