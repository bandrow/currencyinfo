package ru.lid.pet.bank.config;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import ru.lid.pet.bank.dto.CurrencyRateHistoryRequestDto;
import ru.lid.pet.bank.dto.CurrencyRateRequestDto;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.function.Function;

@Configuration
public class AppConfig {

    @Primary
    @Bean
    public WebClient webClient() {
        return WebClient
                .builder()
                .build();
    }

    @Bean
    public Function<InputStream, List<CurrencyRateRequestDto>> parseCurrencyRateDailyCSV() {
          return (inputStream) -> new CsvToBeanBuilder<CurrencyRateRequestDto>(new InputStreamReader(inputStream))
                    .withSeparator('|')
                    .withSkipLines(1)
                    .withIgnoreLeadingWhiteSpace(true)
                    .withType(CurrencyRateRequestDto.class)
                    .build()
                    .parse();
    }

    @Bean
    public Function<InputStream, List<CurrencyRateHistoryRequestDto>> parseCurrencyRateHistoryCSV() {
        return (inputStream) -> new CsvToBeanBuilder<CurrencyRateHistoryRequestDto>(new InputStreamReader(inputStream))
                .withSeparator('|')
                .withIgnoreLeadingWhiteSpace(true)
                .withType(CurrencyRateHistoryRequestDto.class)
                .build()
                .parse();
    }
}
