package ru.lid.pet.bank.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties
@Getter
public class CurrencyRateApiProperties {
    @Value("${integration.bank.host}")
    private String host;

    @Value("${integration.bank.api.currency.rate}")
    private String apiCurrencyRate;

    @Value("${integration.bank.api.history.date}")
    private String apiHistoryRate;
}
