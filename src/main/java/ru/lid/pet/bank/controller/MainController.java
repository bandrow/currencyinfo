package ru.lid.pet.bank.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import ru.lid.pet.bank.dto.CurrencyRateResponseDto;
import ru.lid.pet.bank.service.IntegrationBankService;

@RestController
@RequestMapping("/v1.0/currency-rate")
@RequiredArgsConstructor
public class MainController {
    private final IntegrationBankService integrationBankService;

    @GetMapping("/daily")
    public Flux<CurrencyRateResponseDto> getCurrencyRate() {
        return integrationBankService.getCurrencyRate();
    }

    @GetMapping("/history")
    public Flux<String> getHistoryData() {
        integrationBankService.getHistoryCurrencyData();
        return Flux.just("Ok");
    }
}
