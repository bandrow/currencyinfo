package ru.lid.pet.bank.dto;

import com.opencsv.bean.CsvBindByName;
import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyRateRequestDto {
    @CsvBindByName(column = "Country")
    private String country;
    @CsvBindByName(column = "Currency")
    private String currency;
    @CsvBindByName(column = "Amount")
    private Integer amount;
    @CsvBindByName(column = "Code")
    private String code;
    @CsvBindByName(column = "Rate")
    private Double rate;
}
