package ru.lid.pet.bank.dto;

import com.opencsv.bean.CsvBindAndJoinByName;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;
import com.opencsv.bean.CsvIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.collections4.MultiValuedMap;

import java.time.LocalDate;
import java.util.Map;

@Data
@NoArgsConstructor
@ToString
public class CurrencyRateHistoryRequestDto {
    @CsvDate(value = "dd.MM.yyyy")
    @CsvBindByName(column = "Date")
    private LocalDate date;

    @CsvBindAndJoinByName(column = ".*", elementType = String.class)
    private MultiValuedMap<String, String> line;
}

