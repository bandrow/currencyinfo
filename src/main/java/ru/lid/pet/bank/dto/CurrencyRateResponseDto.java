package ru.lid.pet.bank.dto;

import lombok.Builder;

@Builder
public record CurrencyRateResponseDto(
        String country,
        String currency,
        Integer amount,
        String code,
        Double rate
) {
}
