package ru.lid.pet.bank.service.impl;

import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import ru.lid.pet.bank.config.CurrencyRateApiProperties;
import ru.lid.pet.bank.db.model.CurrencyInfoEntity;
import ru.lid.pet.bank.db.model.CurrencyRate;
import ru.lid.pet.bank.db.repository.CurrencyRateRepository;
import ru.lid.pet.bank.dto.CurrencyRateHistoryRequestDto;
import ru.lid.pet.bank.dto.CurrencyRateRequestDto;
import ru.lid.pet.bank.dto.CurrencyRateResponseDto;
import ru.lid.pet.bank.service.CurrencyService;
import ru.lid.pet.bank.service.IntegrationBankService;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class IntegrationBankServiceImpl implements IntegrationBankService {
    private final CurrencyRateApiProperties currencyRateApiProperties;
    private final WebClient webClient;
    private final Function<InputStream, List<CurrencyRateRequestDto>> parseCurrencyRateDailyCSV;
    private final Function<InputStream,List<CurrencyRateHistoryRequestDto>> parseCurrencyRateHistoryCSV;
    private final CurrencyService currencyService;

    @Override
    public Flux<CurrencyRateResponseDto> getCurrencyRate() {
        return webClient
                .get()
                .uri(String.format(currencyRateApiProperties.getApiCurrencyRate(), currencyRateApiProperties.getHost(), "27.07.2005"))
                .exchangeToMono(clientResponse -> clientResponse.bodyToMono(String.class))
                .doOnNext(System.out::println)
                .map(msg -> parseCurrencyRateDailyCSV.apply(new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8))))
                .flatMapMany(Flux::fromIterable)
                .map(currencyRateRequestDto -> CurrencyInfoEntity.builder()
                        .country(currencyRateRequestDto.getCountry())
                        .currency(currencyRateRequestDto.getCurrency())
                        .code(currencyRateRequestDto.getCode())
                        .newCurrency(true)
                        .build())
                .collectList()
                .flatMapMany(currencyService::saveCurrencyInfoIfNotExist)
                .map(currencyInfoEntity -> CurrencyRateResponseDto.builder()
                        .country(currencyInfoEntity.getCountry())
                        .currency(currencyInfoEntity.getCurrency())
                        .code(currencyInfoEntity.getCode())
                        .build()
                );
    }

    @Override
    public void getHistoryCurrencyData() {
        webClient
                .get()
                .uri(String.format(currencyRateApiProperties.getApiHistoryRate(), currencyRateApiProperties.getHost(), "2022"))
                .exchangeToFlux(clientResponse -> clientResponse.bodyToFlux(String.class))
                .collectList()
                .map(csvLines -> {
                    List<List<String>> result = new ArrayList<>();
                    List<String> subList = new ArrayList<>();

                    for (String item: csvLines) {
                        if (item.startsWith("Date") && subList.size() > 0) {
                            result.add(subList);
                            subList = new ArrayList<>();
                            subList.add(item);
                        } else {
                            subList.add(item);
                        }
                    }
                    result.add(subList);
                    return result;
                })
                .flatMapMany(Flux::fromIterable)
                .map(oneCSV ->  parseCurrencyRateHistoryCSV.apply(
                            new ByteArrayInputStream(Strings.join(oneCSV, '\n')
                                    .getBytes(StandardCharsets.UTF_8))
                        )
                )
                .flatMap(Flux::fromIterable)
                .flatMap(currencyRateHistoryRequestDto -> currencyService.saveCurrencyRateIfNotExist (
                        getAllCurrencyRate(currencyRateHistoryRequestDto)
                ))
                .subscribe();
    }

    private List<CurrencyRate> getAllCurrencyRate(CurrencyRateHistoryRequestDto currencyRateHistoryRequestDto) {
        return currencyRateHistoryRequestDto.getLine().entries().stream()
                .filter(entry -> !entry.getKey().equals("Date"))
                .map(entry -> CurrencyRate.builder()
                        .date(currencyRateHistoryRequestDto.getDate())
                        .amount(Integer.parseInt(
                                entry.getKey().substring(0, entry.getKey().indexOf(' '))
                        ))
                        .code(entry.getKey().substring(entry.getKey().indexOf(' ')))
                        .rate(Double.valueOf(entry.getValue()))
                        .build()
                )
                .collect(Collectors.toList());
    }
}
