package ru.lid.pet.bank.service;

import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Flux;
import ru.lid.pet.bank.dto.CurrencyRateRequestDto;
import ru.lid.pet.bank.dto.CurrencyRateResponseDto;

public interface IntegrationBankService {
    Flux<CurrencyRateResponseDto> getCurrencyRate();
    void getHistoryCurrencyData();
}
