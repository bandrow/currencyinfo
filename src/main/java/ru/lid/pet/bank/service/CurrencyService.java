package ru.lid.pet.bank.service;

import reactor.core.publisher.Flux;
import ru.lid.pet.bank.db.model.CurrencyInfoEntity;
import ru.lid.pet.bank.db.model.CurrencyRate;

import java.util.List;

public interface CurrencyService {
    Flux<CurrencyInfoEntity> saveCurrencyInfoIfNotExist(List<CurrencyInfoEntity> currencyInfoEntities);

    Flux<CurrencyRate> saveCurrencyRateIfNotExist(List<CurrencyRate> currencyRates);
}
