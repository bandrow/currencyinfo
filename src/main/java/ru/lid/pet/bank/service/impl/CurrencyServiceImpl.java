package ru.lid.pet.bank.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.lid.pet.bank.db.model.CurrencyInfoEntity;
import ru.lid.pet.bank.db.model.CurrencyRate;
import ru.lid.pet.bank.db.repository.CurrencyInfoRepository;
import ru.lid.pet.bank.db.repository.CurrencyRateRepository;
import ru.lid.pet.bank.service.CurrencyService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class CurrencyServiceImpl implements CurrencyService {
    private final CurrencyRateRepository currencyRateRepository;
    private final CurrencyInfoRepository currencyInfoRepository;

    @Transactional
    @Override
    public Flux<CurrencyRate> saveCurrencyRateIfNotExist(List<CurrencyRate> currencyRates) {
        return Flux.fromIterable(currencyRates)
                .flatMap(this::getSavedEntity)
                .flatMap(currencyRate -> currencyInfoRepository.findCurrencyInfoEntityByCode(currencyRate.getCode())
                        .map(currencyInfoEntity -> {
                            currencyRate.setCurrencyInfoId(currencyInfoEntity.getId());
                            return currencyRate;
                        })
                        .hasElement()
                        .map(aBoolean -> {
                            System.out.println(aBoolean);
                            return currencyRate;
                        })
                )
                .flatMap(x -> {
                    System.out.println(x);
                    return currencyRateRepository.save(x);
                });
    }

    @Transactional
    @Override
    public Flux<CurrencyInfoEntity> saveCurrencyInfoIfNotExist(List<CurrencyInfoEntity> currencyInfoEntities) {
        return Flux.fromIterable(currencyInfoEntities)
                .flatMap(this::getSavedEntity)
                .flatMap(currencyInfoRepository::save);
    }

    public Mono<CurrencyInfoEntity> getSavedEntity(CurrencyInfoEntity currencyInfoEntity) {
       return currencyInfoRepository.findCurrencyInfoEntityByCode(currencyInfoEntity.getCode())
               .hasElement()
               .flatMap(aBoolean -> {
                   if (!aBoolean) {
                       currencyInfoEntity.setNewCurrency(true);
                       return Mono.just(currencyInfoEntity);
                   }
                   return Mono.empty();
               });
    }

    public Mono<CurrencyRate> getSavedEntity(CurrencyRate currencyRate) {
        return currencyRateRepository.findCurrencyRateByDateAndCodeAndAmount(currencyRate.getDate(), currencyRate.getCode(), currencyRate.getAmount())
                .hasElement()
                .flatMap(aBoolean -> {
                    if (!aBoolean) {
                        currencyRate.setCurrencyRateNew(true);
                        return Mono.just(currencyRate);
                    }
                    return Mono.empty();
                });
    }
}
